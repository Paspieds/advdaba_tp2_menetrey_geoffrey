from neo4j import GraphDatabase
import json, re
from bson import json_util
import time
import sys

def read_bson_to_json(filename):
    json_string = ""
    with open(filename, 'r') as f:
        for line in f:
            json_line = re.sub(r'NumberInt\([^\d]*(\d+)[^\d]*\)', r'\1', line)
            json_string += json_line
        data = json.loads(json_string, object_hook=json_util.object_hook)
        return data
 
class PopulateNeo4j:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

    def create_article(self, id, title, authors, references):

        if id == None or title == None:
            return
        
        title = title.replace("'", "\\'")

        with self.driver.session() as session:
            session.run(
                'OPTIONAL MATCH (a:Article {_id:"%s"}) SET a.title = \'%s\' WITH a WHERE a IS NULL\n' % (id, title) + 
                'CREATE (a2:Article {_id:"%s", title:\'%s\'})' % (id, title))
            
            if references != None:
                for reference in references:
                    self.create_cite_relation(session, id, reference)

            if authors != None:
                for author in authors:
                    self.create_author(session, id, author.get("_id", None), author.get("name", None))

    def create_author(self, session, id, id_author, name):

        if name == None:
            return
    
        name = name.replace("'", "\\'")

        session.run(
            'OPTIONAL MATCH (a:Author {name:\'%s\'}) SET a.name = \'%s\' WITH a WHERE a IS NULL\n' % (id_author, name) +
            'CREATE (a2:Author {_id:"%s", name:\'%s\'})' % (id_author, name))
            
        self.create_authored_relation(session, id, name)

    def create_cite_relation(self, session, id, reference):
        session.run(
            'OPTIONAL MATCH (a:Article {_id:"%s"}) WITH CASE WHEN a IS NULL THEN false ELSE true END AS nodeExist\n' % (reference) +
            'WITH nodeExist\n' +
            'WHERE nodeExist = true\n' +
            'MATCH (a1:Article {_id:"%s"}) WITH a1\n' % (id) +
            'MATCH (a2:Article {_id:"%s"}) WITH a1, a2\n' % (reference) +
            'CREATE (a1)-[:CITES]->(a2)')
        
        session.run(
            'OPTIONAL MATCH (a:Article {_id:"%s"}) WITH CASE WHEN a IS NULL THEN false ELSE true END AS nodeExist\n' % (reference) +
            'WITH nodeExist\n' +
            'WHERE nodeExist = false\n' +
            'MATCH (a1:Article {_id:"%s"}) WITH a1\n' % (id) +
            'CREATE (a2:Article {_id:"%s"}) WITH a1, a2\n' % (reference) +
            'CREATE (a1)-[:CITES]->(a2)')
        
    def create_authored_relation(self, session, id, name):

        session.run(
            'MATCH (ar:Article {_id:"%s"}) WITH ar\n' % (id) +
            'MATCH (au:Author {name:\'%s\'}) WITH au, ar\n' % (name) +
            'CREATE (au)-[:AUTHORED]->(ar)')

        """session.run(
            'OPTIONAL MATCH (a:Author {name:\'%s\'}) WITH CASE WHEN a IS NULL THEN false ELSE true END AS nodeExist\n' % (name) +
            'WITH nodeExist\n' +
            'WHERE nodeExist = true\n' +
            'MATCH (ar:Article {_id:"%s"}) WITH ar\n' % (id) +
            'MATCH (au:Author {name:\'%s\'}) WITH au, ar\n' % (name) +
            'CREATE (au)-[:AUTHORED]->(ar)')
        
        session.run(
            'OPTIONAL MATCH (a:Author {name:\'%s\'}) WITH CASE WHEN a IS NULL THEN false ELSE true END AS nodeExist\n' % (name) +
            'WITH nodeExist\n' +
            'WHERE nodeExist = false\n' +
            'MATCH (ar:Article {_id:"%s"}) WITH ar\n' % (id) +
            'CREATE (au:Author {name:\'%s\'}) WITH au, ar\n' % (name) +
            'CREATE (au)-[:AUTHORED]->(ar)')"""

if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("Don't forget to enter the path of the bson file as an argument")
        raise SystemExit

    bson_file = sys.argv[1]
    print("========= Loading data =========")
    start = time.time()
    data = read_bson_to_json(bson_file)
    print("========= Data loaded =========")

    print("========= Populating neo4j database =========")
    pN4j = PopulateNeo4j("bolt://localhost:7687", "neo4j", "neo4j")
    for article in data:
        id = article.get("_id", None)
        title = article.get("title", None)
        authors = article.get("authors", None)
        references = article.get("references", None)
        pN4j.create_article(id, title, authors, references)
    pN4j.close()
    end = time.time()
    print("========= Neo4j database populated =========")
    print("processed time : " + str(end - start))


