# TP2 advanced database

This the report repo of the second laboratory on neo4j from Geoffrey Menetrey.

repo : https://gitlab.com/Paspieds/advdaba_tp2_menetrey_geoffrey.git

## Disclaimer

Performance test with a limited resources machine with k8s on Rancher has not been tested. The current python script given in this repo is able to read the bson file and create the wanted graph database on neo4j. A small sample of the big bson file is available for test purpose. The script contains a total of 624 articles with this sample.
Since the script has never been tested on the real file, not all the articles are contained. This means that some reference of some articles will create articles nodes without a title. Since the file does contains article that references other articles that will appear next in the file(maybe all through the end), the script create temporary articles with no title. They will be updated with their title when their time comes to be processed. Of course, some will stay without a title with sample files.

## Loading data and performances

The script does not scale very well. To read the bson file, the script first loads the bson file. It then reads one line per one line and replaces all the characters that json file format does not support and finally loads it as a json object. Reading the file in its entirety makes the script crash, this is why each line are processed and then concatenated in a big string. Afterward, the script is able to read the json object to create the wanted nodes and links. This means that the script goes through the file multiple times during its processing.

## Performances enhancement ideas

**The first idea** would be to directly read each line of the loaded bson file and create the nodes without going through the json object. 

**The second adea** would be to optimaze the queries done to the neo4j database to create the Articles and Authors nodes as well as the CITES and AUTHOURED links. It seems that the operations that takes the most time during this script are the queries. In fact the time exponentially grows the more nodes to be created there is. This time seems to be a lot more critical than to load the file.

## Create the database

For this part, we take as acquired that neo4j and docker are installed.

The first step ir to create a docker container with the latest neo4j image :

```bash
docker run -d --name tp2_neo4j -e NEO4J_AUTH=none -p7474:7474 -p7687:7687 \ 
    -v $NEO4J_DB_PATH/neo4jlogs:/logs \ 
    -v $NEO4J_DB_PATH/neo4j/data:/data \ 
    -v $NEO4J_DB_PATH/neo4j/import:/var/lib/neo4j/import neo4j:latest
```
The variable **NEO4J_DB_PATH** has to be replaced with the wanted path for the database file to be stored.

The next step is then to use the script. For the script to work, we need the bson file containing the correct information to create the nodes and links :

```bash
python3 bson_to_n4j_db.py $BSON_FILE_PATH
```

the variable **BSON_FILE_PATH** has to be replaced with the bson_file.

The script will then processed the file and create the database as explained in the **Performance** part. At the end, the script will display the time spent for all the operations to be processed.